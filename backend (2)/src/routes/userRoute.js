const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");

// mengambil data user
router.get("/users", userController.getAllUser);
router.get("/user/:email&:password", userController.getUser);

// merubah sebagian data user
router.put("/user/:id", userController.updateUser);

// menambah data user
router.post("/user/", userController.createUser);

// menghapus user
router.delete("/user/:id", userController.deleteUser);

module.exports = router;
